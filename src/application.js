var React = require("react");
var ReactDOM = require("react-dom");

class ClickableTitle extends React.Component {

    constructor(props) {
        super(props);
        this.state = { count: 0 }; // l'état initial de notre composant
        this.incrementCount = this.incrementCount.bind(this);
    }

    incrementCount() {
        // quand l'utilisateur clique sur le titre
        this.setState({ count: this.state.count + 1 });
    }

    render() {
        return (
            <h1 onClick={ this.incrementCount.bind(this) }>
                { this.props.title }
                (cliqué { this.state.count } fois)
            </h1>
        );
    }

}

ReactDOM.render(<ClickableTitle title="Hello World"/>, document.getElementById("main"));
